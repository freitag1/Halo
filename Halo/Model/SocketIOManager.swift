//
//  SocketIOManager.swift
//  Halo
//
//  Created by 김병일 on 2022/12/23.
//

import Foundation
import SocketIO


class SocketIOManager: NSObject {
    static let shared = SocketIOManager()
    var manager = SocketManager(socketURL: URL(string: "ws://127.0.0.1:5006")!, config: [.log(true), .compress])
    var socket: SocketIOClient!
  
    override init() {
        socket = self.manager.socket(forNamespace: "/")
    }

    func establishConnection() {
        socket.connect()
    }
    
    func closeConnection() {
        socket.disconnect()
    }
    
    func chat_login(id: String){
        socket.emit("login",  ["id" : id])
        
    }
    
    func makeJoinRoom( reciever: String , roomId: String , roomName:String ,  roomOwner: String ,
                       message: String){
      
        socket.emit("makeRoom" ,["reciever" : reciever,  "roomId" : roomId , "roomName": roomName , "roomOwner" : roomOwner , "message" : message ] )
    }
    
    func joinRoom( roomid: String , receiver: String){
      
        socket.emit("joinRoom" ,[  "roomid" : roomid  ,"receiver" : receiver ] )
    }
    
    func joinRoom_new( roomid: String , sender : String){
      
        socket.emit("joinRoom" ,[  "roomid" : roomid  ,"sender" : sender ] )
    }
    
    func leaveRoom(){
      
        socket.emit("leaveRoom")
    }
   
    func sendMessage(sender: String , receiver: String , message: String , roomid: String ) {
//        socket.emit("event",  ["message" : "This is a test message"])
//        socket.emit("event1", [["name" : "ns"], ["email" : "@naver.com"]])
//        socket.emit("event2", ["name" : "ns", "email" : "@naver.com"])
//        socket.emit("msg", ["nick": nickname, "msg" : message])
        socket.emit("messageS",  [ "sender": sender, "receiver": receiver ,"message" : message , "roomid" : roomid  ])
    }
    
    func sendMessage_new(sender: String, roomid: String  ,message: String){
        socket.emit("chatMessage",  [ "sender": sender ,"message" : message , "roomid" : roomid  ])
    }
}
