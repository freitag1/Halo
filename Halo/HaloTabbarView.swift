//
//  HaloTabbarView.swift
//  Halo
//
//  Created by 김병일 on 2022/10/21.
//

import SwiftUI

struct HaloTabbarView: View {
    var body: some View {
        TabView {
            if #available(iOS 16.0, *) {
                MainMessagesView()
                    .tabItem {
                        Image(systemName: "message")
                    }
            } else {
                // Fallback on earlier versions
            }
            ProfileView()
                .tabItem {
                    Image(systemName: "person.fill")
                }
        }
    }
}

struct HaloTabbarView_Previews: PreviewProvider {
    static var previews: some View {
        HaloTabbarView()
    }
}
