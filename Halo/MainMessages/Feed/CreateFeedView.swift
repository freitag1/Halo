//
//  CreateFeedView.swift
//  Halo
//
//  Created by 김병일 on 2022/10/08.
//

import SwiftUI
import Alamofire
import PhotosUI

@available(iOS 16.0, *)
struct CreateFeedView: View {
    
    @Environment(\.presentationMode) var presentation
    
    @State private var shoudShowImagePicker = false
    @State var image: UIImage?
    @State private var comments = ""
    @State var placeholderText: String = "당신의 생각을 입력해주세요"
    
    
    @State var maxSeletion: [PhotosPickerItem] = []
    @State var selectedImages: [UIImage] = []
    
    struct TextView: UIViewRepresentable {
        
        typealias UIViewType = UITextView
        var configuration = { (view: UIViewType) in }
        
        func makeUIView(context: UIViewRepresentableContext<Self>) -> UIViewType {
            UIViewType()
        }
        
        func updateUIView(_ uiView: UIViewType, context: UIViewRepresentableContext<Self>) {
            configuration(uiView)
        }
    }
    var body: some View {
        NavigationView {
            VStack {
                    Divider().frame(width: 350, alignment: .center)
                    HStack(spacing: 16 ) {
                                Text("이미지 업로드")
                            .font(.system(size: 15))
                                //Spacer()  //왼쪽 정렬이 되도록
                            }
                    ZStack {
                        
                        if selectedImages.count > 0  {
                            ScrollView(.horizontal) {
                                HStack {
                                    ForEach(selectedImages , id: \.self) { img in
                                        PhotosPicker(selection: $maxSeletion , maxSelectionCount: 10 , matching: .any(of: [.images, .not(.videos)])) {
                                            Image(uiImage: img)
                                                .resizable()
                                                .scaledToFill()
                                                .frame(width: 120 , height: 120)
                                                .clipped()
                                                .padding()
                                        }
                                           
                                    }
                                }
                            }
                            
                        } else {
                            PhotosPicker(selection: $maxSeletion , maxSelectionCount: 10 , matching: .any(of: [.images, .not(.videos)])) {
                                Rectangle().fill(Color.yellow)
                            }
                            VStack{
                                Image(systemName: "plus.circle").foregroundColor(.gray)
                                Text("박스를 클릭하여 이미지를 선택하세요!").foregroundColor(.gray)
                            }
                        }
                    }
                    .frame(width: 350, height: 200)
                    .contentShape(Rectangle())
                    .background(Color.clear)
                    .onChange(of: maxSeletion) { newValue in
                        Task {
                            selectedImages = []
                            for value in newValue {
                                if let imageData = try? await
                                    value.loadTransferable(type: Data.self) ,
                                   let image = UIImage(data: imageData) {
                                    selectedImages.append(image)
                                }
                            }
                        }
                    }
//                    .onTapGesture {
//                        shoudShowImagePicker.toggle()
//
//                    }
                if self.comments.isEmpty {
                  TextEditor(text: $placeholderText)
                    .font(.body)
                    .foregroundColor(.gray)
                    .disabled(true)
                    .padding()
                    .foregroundColor(.black)
                }
                TextEditor(text: $comments)
                  .font(.body)
                  .opacity(self.comments.isEmpty ? 0.25 : 1)
                  .padding()
                  .foregroundColor(.black)
                    Button {
                        createPost()
                    } label: {
                        HStack{
                            Spacer()
                            Text("등록하기")
                                .foregroundColor(.white)
                                .padding(.vertical,10)
                                .font(.system(size: 14, weight: .semibold))
                            Spacer()
                        }.background(Color.blue)
                    }
                  }.navigationTitle("게시글 작성")
                .navigationBarTitleDisplayMode(.inline)
                .navigationBarItems(leading: Button(action: {
                    presentation.wrappedValue.dismiss()
                }, label: {
                    Image(systemName: "chevron.backward").foregroundColor(.black)
                })
                )
            
        }
//                .fullScreenCover(isPresented:   $shoudShowImagePicker , onDismiss: nil) {
//                    ImagePicker(image : $image)
//                }
//        VStack {
//            Button(action: {
//                            presentation.wrappedValue.dismiss()
//                        }) {
//                            Text("Modal view 닫기").bold()
//                        }
//                        .frame(width: 150, height: 30, alignment: .leading)
//                        .background(Color.blue)
//                        .font(.system(size: 16))
//                        .foregroundColor(Color.white)
//            TextField("Comments" , text: $comments).padding(12) .frame(width: 150, height: 150, alignment: .center)
//                .background(Color.white)
//             Button {
//                 shoudShowImagePicker.toggle()
//             } label: {
//                 VStack{
//                     if let image = self.image {
//                         Image(uiImage: image)
//                             .resizable()
//                             .scaledToFill()
//                             .frame(width: 128, height: 128)
//                             .cornerRadius(64)
//                     } else {
//                         Image(systemName: "person.fill").font(.system(size: 64)).padding().foregroundColor(Color(.label))
//                    }
//                 }
//                 .overlay(RoundedRectangle(cornerRadius: 64).stroke(Color.black , lineWidth: 3))
//             }
//
//            Button {
//                createPost()
//
//            } label: {
//                Text("uplaod feed").bold()
//            }.frame(width: 150, height: 30, alignment: .center)
//                .background(Color.blue)
//                .font(.system(size: 16))
//                .foregroundColor(Color.white)
//        }
//        .fullScreenCover(isPresented:   $shoudShowImagePicker , onDismiss: nil) {
//            ImagePicker(image : $image)
//        }
        
    }
    
    private func createPost() {
        print("Create Post")
        
        print(selectedImages)
        
        Service.shared.uplaodPost(comments: comments, imgData: selectedImages) { (res) in
            switch res {
            case .failure(let err):
                print("Failed to upload post  " , err)
            case .success:
                print("upload post in Successfully")

            }
        }
        
        presentation.wrappedValue.dismiss()
        
    }
    
    
}

struct CreateFeedView_Previews: PreviewProvider {
    static var previews: some View {
        if #available(iOS 16.0, *) {
            CreateFeedView()
        } else {
            // Fallback on earlier versions
        }
    }
}
