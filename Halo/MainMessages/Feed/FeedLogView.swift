//
//  FeedLogView.swift
//  Halo
//
//  Created by 김병일 on 2022/09/24.
//

import SwiftUI
import SDWebImageSwiftUI

struct Post1: Identifiable {
    
    var id: Int
    let username , message , imagename, imagetime : String
}

class FeedLogViewModel: ObservableObject {
    
    @Published var posts = [Post]()  // post test
    @Published private(set) var isLoading = false
    
    init() {
        fetchPosts()
    }
    
    fileprivate func fetchPosts() {
        Service.shared.fetchPosts { (res) in
            switch res {
            case .failure(let err):
                print("Failed to fetch post" , err)
            case .success(let posts):
                print("posts feed view ")
                 print(posts)
                self.posts = posts
            }
        }
    }
}

struct PullToRefresh: View {
    
    var coordinateSpaceName: String
    var onRefresh: ()->Void
    
    @State var needRefresh: Bool = false
    
    var body: some View {
        GeometryReader { geo in
            if (geo.frame(in: .named(coordinateSpaceName)).midY > 50) {
                Spacer()
                    .onAppear {
                        needRefresh = true
                    }
            } else if (geo.frame(in: .named(coordinateSpaceName)).maxY < 10) {
                Spacer()
                    .onAppear {
                        if needRefresh {
                            needRefresh = false
                            onRefresh()
                        }
                    }
            }
            HStack {
                Spacer()
                if needRefresh {
                    ProgressView()
                } else {
                   // Text("⬇️")
                }
                Spacer()
            }
        }.padding(.top, -50)
    }
}

struct FeedLogView: View {
    let user : User?
    @State var selectedIndex = 0
    @ObservedObject private var vm = FeedLogViewModel()
  
 
    let posts: [Post1] = [
        .init(id: 0 , username: "kim byung il"  , message: "성준아 반가워" , imagename: "sj01" , imagetime: "10 days ago"),
        .init(id: 1 ,username: "lee jung sun"  , message: "이런 멋진 미소가?" , imagename: "02" , imagetime: "9 days ago"),
        .init(id: 2 ,username: "kim byung il"  , message: "씩씩한 성준이" , imagename: "03" , imagetime: "10 days ago"),
        .init(id: 3 ,username: "kim byung il"  , message: "항상 건강해~", imagename: "04" , imagetime: "10 days ago"),
        .init(id: 4 ,username: "kim byung il"  , message: "방가방가" , imagename: "05" , imagetime: "10 days ago")
    ]
    
    var body: some View {
        ScrollView {
            PullToRefresh(coordinateSpaceName: "pullToRefresh") {
                    // do your stuff when pulled
                vm.fetchPosts()
                }
            PostView
            .navigationTitle(user?.NAME ?? "")
            .navigationBarTitleDisplayMode(.inline)
        }.coordinateSpace(name: "pullToRefresh")
    }
    
    let tabBarImageNames = ["hand.thumbsup" , "bubble.right" , "arrowshape.turn.up.right"]
    
    private var PostView: some View {
            ForEach(vm.posts , id: \.POST_ID) { post in
                        VStack(alignment: .leading, spacing: 20){
                            HStack {
                                WebImage(url: URL(string: post.PROFILEIMAGE ?? ""  ))
                                    .resizable()
                                    .frame(width: 60, height: 60)
                                    .clipped()
                                    .clipShape(Circle() )
                                    .overlay(Circle().stroke(Color.black, lineWidth: 1  ))
                                    .padding(.leading , 4)
                                VStack(alignment: .leading , spacing: 4) {
                                    Text(post.NAME).font(.headline)
                                    Text(post.timeAgo).font(.subheadline)
                                }.padding(.leading , 8)
                                Spacer()
                                Button {
                                    
                                } label: {
                                    Image(systemName: "ellipsis").foregroundColor(.black)
                                }.padding(.trailing , 30)
                            }.padding(.leading, 20).padding(.top, 16)
                            Text(post.COMMENTS).padding(.leading , 25).padding(.trailing , 36).lineLimit(nil)
                            WebImage(url: URL(string: post.PROFILEIMAGE ?? ""  )).resizable().frame(height: 350)
//                            ZStack {
//                                ScrollView(.horizontal) {
//                                    HStack {
//                                        ForEach(post.POSTIMAGE , id: \.self) { img in                                                Image(uiImage: img)
//                                                    .resizable()
//                                                    .scaledToFill()
//                                                    .frame(width: 120 , height: 120)
//                                                    .clipped()
//                                                    .padding()fhs
//
//                                        }
//                                    }
//                                }
//
//                            }
                            Divider()
                            HStack{
                                ForEach(0..<3) { num in
                                    Button(action: {
                                        if num == 1 {
                                            return
                                        }
                                        selectedIndex = num }, label:
                                            {
                                        Spacer()
                                        if num == 1 {
                                            Image(systemName: tabBarImageNames[num])
                                                .font(.system(size: 24 , weight: .bold))
                                                .foregroundColor(.black)
                                            Spacer()
                                        }
                                        else
                                        {        Image(systemName: tabBarImageNames[num])
                                                .font(.system(size: 24 , weight: .bold))
                                                .foregroundColor( Color(.black) )
                                            Spacer()}
                                    })
                                }
                            }
                        }.padding(.leading, -20).padding(.bottom, -8).padding(.trailing, -20)
            }.padding(.bottom, 50)
    }
}

//struct PostView: View {
//
//    let post : Post1
//    var body: some View {
//
//        VStack(alignment: .leading, spacing: 16){
//            HStack {
//                Image("sj01")
//                    .resizable()
//                    .frame(width: 60, height: 60)
//                    .clipped()
//                    .clipShape(Circle() )
//                    .overlay(Circle().stroke(Color.black, lineWidth: 1  ))
//                VStack(alignment: .leading , spacing: 4) {
//                    Text(post.username).font(.headline)
//                    Text(post.imagetime).font(.subheadline)
//                }.padding(.leading , 8)
//            }.padding(.leading, 16).padding(.top, 16)
//            Text(post.message).padding(.leading , 16).padding(.trailing , 36).lineLimit(nil)
//            Image(post.imagename).resizable().frame(width: 350, height: 350)
//
//        }.padding(.leading, -20).padding(.bottom, -8).padding(.trailing, -20)
//
//    }
//}


struct FeedLogView_Previews: PreviewProvider {
    static var previews: some View {
        FeedLogView(user: nil)
    }
}


