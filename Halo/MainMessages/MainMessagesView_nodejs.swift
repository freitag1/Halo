////
////  MainMessagesView_nodejs.swift
////  Halo
////
////  Created by 김병일 on 2022/10/08.
////
//
//
//import SwiftUI
//import SDWebImageSwiftUI
//import Firebase
//import FirebaseFirestoreSwift
//import Alamofire
//
//
//class  MainMessagesViewModel: ObservableObject {
//
//    @Published var errorMessage = ""
//    @Published var chatUser : ChatUser?
//    
//    @Published var isUserCurrentlyLoggedOut = false
//
//    init() {
//
//        DispatchQueue.main.async {
//            self.isUserCurrentlyLoggedOut =   FirebaseManager.shared.auth.currentUser?.uid == nil
//        }
//
//        fetchCurrentUser()
//
//        fetchRecentMessages()
//    }
//
//    @Published var recentMessages = [RecentMessage]()
//
////    private var firestoreListener: ListenerRegistration?
//
//    func fetchRecentMessages(){
//
////        guard let uid = FirebaseManager.shared.auth.currentUser?.uid else { return}
////
////        firestoreListener?.remove()
////        self.recentMessages.removeAll()
////
////        firestoreListener = FirebaseManager.shared.firestore
////            .collection(FirebaseConstants.recentMessages)
////            .document(uid)
////            .collection(FirebaseConstants.messages)
////            .order(by: FirebaseConstants.timestamp)
////            .addSnapshotListener { querySnapshot, error in
////                if let error = error {
////                    self.errorMessage = "Failed to listen for recent messages \(error)"
////                    print(error)
////                    return
////                }
////
////                querySnapshot?.documentChanges.forEach({ change in
////
////                    let docId = change.document.documentID
////
////                     if let index = self.recentMessages.firstIndex(where: { rm in
////                        return rm.id == docId
////                    }) {
////                        self.recentMessages.remove(at: index)
////                    }
////
////                    do {
////                        if let rm = try? change.document.data(as: RecentMessage.self) {
////                                self.recentMessages.insert(rm, at: 0)
////                            }
////
////                    } catch {
////                        print(error)
////                    }
////
////                })
////            }
//    }
//
//      func fetchCurrentUser(){
//
////        guard let uid =
////                FirebaseManager.shared.auth.currentUser?.uid
////        else {
////
////            self.errorMessage = "Can not fine Firebase uid "
////            return}
////
////        FirebaseManager.shared.firestore.collection("users").document(uid).getDocument
////          { snapshot, error in
////            if let error = error {
////                self.errorMessage = "Failed to fetch current user: \(error)"
////                print("Failed to fetch current user" , error)
////                return
////            }
////
////              self.chatUser = try? snapshot?.data(as: ChatUser.self)
////              FirebaseManager.shared.currentUser = self.chatUser
////        }
//
//    }
//
//    func handleSignOut() {
//        isUserCurrentlyLoggedOut.toggle()
//        //try? FirebaseManager.shared.auth.signOut()
//
//        guard let url = URL(string: "http://localhost:5005/auth/logout") else {
//            return    // 데이터를 보낼 서버 url
//        }
//
//        var request = URLRequest(url: url)
//        request.httpMethod = "GET"         // http 메서드는 'POST'
//
////        do { // request body에 전송할 데이터 넣기
////            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
////        } catch {
////            print(error.localizedDescription)
////        }
////
////        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
////        request.addValue("application/json", forHTTPHeaderField: "Accept-Type")
//
//        let session = URLSession.shared
//        session.dataTask(with: request, completionHandler: { (data, resp, err) in
//                if let err = err {
//
//                    print(" 44444")
//                    print("Failed to logout nodejs server" , err)
//                    return
//                }
//
//                print(" resp : " , resp)
//
//                print(" probably Success logout  ")
//
//           // self.didCompleteLoginProcess()
//
//          //  shoudShowMainMessageScreen.toggle()
//
//        }).resume()
//    }
//}
//
//struct MainMessagesView_nodejs: View {
//
//    @State var shouldShowLogOutOptions = false
//
//    @State var shoudNavigateChatLogView = false  //single chat log view
//    
//    @State var shoudShowModel = false  //create Feed View
//    
//    @ObservedObject private var vm = MainMessagesViewModel()
//
//    private var chatLogViewModel = ChatLogViewModel(chatUser: nil  )
//    
//    @State var selectedIndex = 0
//    
//    @State var image: UIImage?
//    
//    let tabBarImageNames = ["message" , "plus.app.fill" , "photo"]
//
//    var body: some View {
//        NavigationView {
//
//            VStack {
//                customNavBar
//                messagesView
//
//                NavigationLink( "", isActive: $shoudNavigateChatLogView) {
//                //  HaloTabbarView()
//                //  ChatLogView(vm: chatLogViewModel  )
//                // Text(" tab chat view")
//                 //   HaloTabbarView(chatUser: self.chatUser)
//                    
//                    VStack {
//                        ZStack {
//                            Spacer().fullScreenCover(isPresented: $shoudShowModel , content: {
//                                Button(action: {shoudShowModel.toggle()}, label: {
//                                    ImagePicker(image : $image)
//                                })
//                            }
//                            )
//                            
//                            switch selectedIndex {
//                            case 0 :
//                                ChatLogView(vm: chatLogViewModel  )
//                            case 2 :
//                                FeedLogView()
//                            default :
//                                ChatLogView(vm: chatLogViewModel  )
//                            }
//                        }
//                        
//                        Spacer()
//                        
//                        HStack{
//                            ForEach(0..<3) { num in
//                                Button(action: {
//                                    
//                                    if num == 1 {
//                                        shoudShowModel.toggle()
//                                        return
//                                    }
//                                    
//                                    selectedIndex = num }, label:
//                                        {
//                                    Spacer()
//                                    
//                                    if num == 1 {
//                                        Image(systemName: tabBarImageNames[num])
//                                            .font(.system(size: 44 , weight: .bold))
//                                            .foregroundColor(.red)
//                                        Spacer()
//                                    }
//                                    else
//                                        
//                                    {        Image(systemName: tabBarImageNames[num])
//                                            .font(.system(size: 24 , weight: .bold))
//                                            .foregroundColor(selectedIndex == num ? Color(.black) : .init(white: 0.8))
//                                        Spacer()}
//                            
//                                    
//                                })
//                                   
//                            }
//                        }
//                
//                        
//                    }
//                    
//                 
//                    
//                    
////                    TabView {
////                        ChatLogView(vm: chatLogViewModel  )  //chat log View
////                            .tabItem {
////                                Image(systemName: "message")
////                                Text("채팅")
////                            }
////
////                        FeedLogView()
////                            .tabItem {
////                                Image(systemName: "photo")
////                                Text("사진스토리")
////                            }
////                    }
//                }
//            }
//            .overlay(
//                newMessageButton, alignment: .bottom)
//            .navigationBarHidden(true)
//        }
//    }
//
//    private var customNavBar: some View {
//        HStack(spacing: 16) {
//
//            WebImage(url: URL(string: vm.chatUser?.profileImageUrl ?? ""  ))
//                .resizable()
//                .scaledToFill()
//                .frame(width: 50, height: 50)
//                .clipped()
//                .cornerRadius(50)
//                .overlay(RoundedRectangle(cornerRadius: 44)
//                            .stroke(Color(.label), lineWidth: 1)
//                )
//                .shadow(radius: 5  )
//
////
//
//            VStack(alignment: .leading, spacing: 4) {
//
//                let email = vm.chatUser?.email.replacingOccurrences(of:
//                    "@naver.com", with: "") ?? ""
//                Text(email)
//                    .font(.system(size: 24, weight: .bold))
//
//                HStack {
//                    Circle()
//                        .foregroundColor(.green)
//                        .frame(width: 14, height: 14)
//                    Text("online")
//                        .font(.system(size: 12))
//                        .foregroundColor(Color(.lightGray))
//                }
//
//            }
//
//            Spacer()
//            Button {
//                shouldShowLogOutOptions.toggle()
//            } label: {
//                Image(systemName: "gear")
//                    .font(.system(size: 24, weight: .bold))
//                    .foregroundColor(Color(.label))
//            }
//        }
//        .padding()
//        .actionSheet(isPresented: $shouldShowLogOutOptions) {
//            .init(title: Text("Settings"), message: Text("What do you want to do?"), buttons: [
//                .destructive(Text("Sign Out"), action: {
//                    print("handle sign out")
//
//                    vm.handleSignOut()
//                }),
//                    .cancel()
//            ])
//        }
//        .fullScreenCover(isPresented: $vm.isUserCurrentlyLoggedOut, onDismiss: nil){
//            LoginView_nodejs(didCompleteLoginProcess: {
//                self.vm.isUserCurrentlyLoggedOut = false
//                self.vm.fetchCurrentUser()
//                self.vm.fetchRecentMessages()
//            })
//        }
//    }
//
//
//
//    private var messagesView: some View {
//        ScrollView {
//            ForEach(vm.recentMessages) { recentMessage in
//                VStack {
//                    Button {
////                        let uid = FirebaseManager.shared.auth.currentUser?.uid == recentMessage.fromId ? recentMessage.toId : recentMessage.fromId
////
////                        self.chatUser = .init(id: uid, uid: uid, email: recentMessage.email, profileImageUrl: recentMessage.profileImageUrl)
////
////                        self.chatLogViewModel.chatUser = self.chatUser
////                        self.chatLogViewModel.fetchMessages()
////                        self.shoudNavigateChatLogView.toggle()
//                    } label: {
//                        HStack(spacing: 16) {
//                            WebImage(url: URL(string: recentMessage.profileImageUrl))
//                                .resizable()
//                                .scaledToFill()
//                                .frame(width: 64, height: 64)
//                                .clipped()
//                                .cornerRadius(64)
//                                .overlay(RoundedRectangle(cornerRadius: 64)
//                                            .stroke(Color.black, lineWidth: 1))
//                                .shadow(radius: 5)
//
//
//                            VStack(alignment: .leading, spacing: 8) {
//                                Text(recentMessage.username)
//                                    .font(.system(size: 16, weight: .bold))
//                                    .foregroundColor(Color(.label))
//                                    .multilineTextAlignment(.leading)
//                                Text(recentMessage.text)
//                                    .font(.system(size: 14))
//                                    .foregroundColor(Color(.darkGray))
//                                    .multilineTextAlignment(.leading)
//                            }
//                            Spacer()
//
//                            Text(recentMessage.timeAgo)
//                                .font(.system(size: 14, weight: .semibold))
//                                .foregroundColor(Color(.label))
//                        }
//                    }
//
//
//
//                    Divider()
//                        .padding(.vertical, 8)
//                }.padding(.horizontal)
//
//            }.padding(.bottom, 50)
//        }
//    }
//
//    @State var shouldShowNewMassegeScreen = false
//
//    private var newMessageButton: some View {
//        Button {
//
//            shouldShowNewMassegeScreen.toggle()
//
//        } label: {
//            HStack {
//                Spacer()
//                Text("+ New Message")
//                    .font(.system(size: 16, weight: .bold))
//                Spacer()
//            }
//            .foregroundColor(.white)
//            .padding(.vertical)
//                .background(Color.blue)
//                .cornerRadius(32)
//                .padding(.horizontal)
//                .shadow(radius: 15)
//        }
//        .fullScreenCover(isPresented: $shouldShowNewMassegeScreen){
//            CreateNewMessageView(didSelectNewUser: {
//
//                user in print(user.email)
//                self.shoudNavigateChatLogView.toggle()  // go to the selected users chat log view
//                self.chatUser = user
//                self.chatLogViewModel.chatUser = user
//                self.chatLogViewModel.fetchMessages()
//
//
//            } )
//        }
//    }
//
//    @State var chatUser : ChatUser?
//}
