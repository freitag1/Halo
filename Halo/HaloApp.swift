//
//  HaloApp.swift
//  Halo
//
//  Created by 김병일 on 2022/05/14.

import SwiftUI


@main
struct HaloApp: App {
    var body: some Scene {
        WindowGroup {
            if #available(iOS 16.0, *) {
                MainMessagesView()
            } else {
                // Fallback on earlier versions
            }
           // HaloTabbarView()
            //LauncherView()
        }
    }
}
