//
//  ChatLogView_new.swift
//  Halo
//
//  Created by 김병일 on 2022/10/22.
//

import SwiftUI
import Alamofire
import Starscream
import SocketIO


        
struct Message: Codable   {
    
    var ID = UUID().uuidString
    let SENDER ,  MESSAGE , ROOMID : String
  
   // let INITDTTM : Date
    
    
//    var timeAgo: String {
//        let formatter = RelativeDateTimeFormatter()
//        formatter.unitsStyle = .abbreviated
//        return formatter.localizedString(for: INITDTTM, relativeTo: Date())
//    }
}

class ChatLogView_new_model: ObservableObject  {
    
    @Published var chatText = ""
    @Published var messages = [Message]()  // nodejs
    @Published var currentUser : User?
    
    var user : User?
    
    init(user : User? , currentUser : User?) {
        self.user = user
        self.currentUser = currentUser
        receivedMessage()
        print("초기화 중입니다.")
        //fetchMessages()
        SocketIOManager.shared.establishConnection()
      }
    
    func fetchMessages() {
        print("fetchMessages")
        print("currentUser : \(currentUser?.EMAIL ?? "" )")
        print("chatUser :\(user?.EMAIL ?? "")")
                            
        guard let fromId = currentUser?.EMAIL else {return}
        guard let toId = user?.EMAIL else {return}
        guard let roomId = user?.ROOMID else {return}

        Service.shared.fetchMessages (fromId: fromId, toId: toId , roomId: roomId) { (res) in
            switch res {
            case .failure(let err):
                print("Failed to fetchmessages" , err)
            case .success(let messages):
                print("Successfully fetchmessages in chatview")
                 //print(messages)
                self.messages = messages
            }
            
            DispatchQueue.main.async {
                self.count += 1
            }
        }
        
    
    }
    
    func receivedMessage() {
        
        //message received
        
        let socket = SocketIOManager.shared.manager.socket(forNamespace: "/")
        
                socket.off("message")
                socket.on("message") { (data, ack) in

                print("chat Message View")
                print(data)

                let data = data[0] as! NSDictionary
                let sender = data["SENDER"] as! String
                let message = data["MESSAGE"] as! String
                let roomid = data["ROOMID"] as! String
                self.messages.append(Message(SENDER: sender  , MESSAGE: message , ROOMID: roomid  ))
    
                DispatchQueue.main.async {
                    self.count += 1
                }

                }
    }
    
    func handleSend_new(){
        
        print(chatText)
        print(user?.EMAIL ?? "")
        print(currentUser?.EMAIL ?? "")
        print(user?.ROOMID ?? "")
        
        guard let fromId = currentUser?.EMAIL else {return}
        guard let toId = user?.EMAIL else {return}
        guard let roomId = user?.ROOMID else {return}
        
        print(fromId)
        print(toId)
        print(roomId)
    
        SocketIOManager.shared.sendMessage_new(sender: fromId, roomid: roomId , message: chatText)

        self.messages.append(Message(SENDER: fromId, MESSAGE: chatText , ROOMID: roomId ))
        // 내창에 내 입력 내용 추가
        
        Service.shared.saveMessage(fromId: fromId, toId: toId, chatText: chatText , roomId: roomId ){ (res) in
            switch res {
            case .failure(let err):
                print("Failed to save messages:", err)
            case .success(let messages):
                print("Successfully save messages")

            }
        }
    
        self.chatText = ""
        self.count += 1
        
    }
    
    @Published var count = 0
}

struct MenuItem: Identifiable {
    var id = UUID()
    let text: String
}

struct MenuContent: View {
    
    let items : [MenuItem] = [
           MenuItem(text: "add friends")
    
    ]
    
    
    var body: some View {
        ZStack {
            Color(UIColor(red: 33/255.0, green: 33/255.0, blue: 33/255.0, alpha: 1) )
            
            VStack(alignment: .leading, spacing: 0) {
                ForEach(items){ item in
                    
                }
                Spacer()
                
            }
        }
        
    }
}

struct MenuView: View {
    
    let width : CGFloat
    let showingMenu : Bool
    let toggleMenu: () -> Void
    
    var body: some View {
        ZStack {
            GeometryReader { geometry in
                           EmptyView()
                            }
            .background(Color.red.opacity(0.5))
            .opacity(self.showingMenu ? 1 : 0)
            .animation(Animation.easeIn.delay(0.25))
            .onTapGesture {
                self.toggleMenu()
            }
            
            HStack {
                MenuContent()
                    .frame(width: width)
                
                Spacer()
                
            }
            
        }
    }
}

struct ChatLogView_new : View {

    @ObservedObject var vm : ChatLogView_new_model
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @State private var showingMenu = false

     var btnBack : some View { Button(action: {
         goBack()
        
         }) {
             HStack {
             Image("ic_back") // set image here
                 .aspectRatio(contentMode: .fit)
                 .foregroundColor(.white)
                 Text("Go back")
             }
         }
     }
    
    private func goBack() {
        self.presentationMode.wrappedValue.dismiss()
        SocketIOManager.shared.closeConnection()
        SocketIOManager.shared.establishConnection()
    }
    
    func toggleMenu() {
        showingMenu.toggle()
    }

    
    var body: some View {
        ZStack{
           
            messageView_new
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: btnBack)
        .navigationTitle(vm.user?.NAME ?? "") //toId
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarItems(trailing: Button(action: {
                    withAnimation {
                        showingMenu.toggle()
                    }
                }) {
                    Image(systemName: "line.3.horizontal")
                        .resizable()
                        .frame(width: 20, height: 20)
                })
                .background(
                    Group {
                        if showingMenu {
                            Color.black.opacity(0.5)
                                .onTapGesture {
                                    withAnimation {
                                        showingMenu.toggle()
                                    }
                                }
                        }
                    }
                    .transition(.opacity)
                )
                .overlay(
                    ZStack {
                        if showingMenu {
                            Color.black.opacity(0.5)
                                .onTapGesture {
                                    withAnimation {
                                        showingMenu.toggle()
                                    }
                                }
                                .transition(.opacity)
                            
                            VStack {
                                Spacer()
                                
                                MenuView(width: 370, showingMenu: showingMenu, toggleMenu: toggleMenu)
                            }
                            .transition(.move(edge: .trailing))
                        }
                    }
                    .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .bottomTrailing)
                    .gesture(
                        TapGesture()
                            .onEnded {
                                withAnimation {
                                    showingMenu = false
                                }
                            }
                    )
                )
                .animation(.easeInOut) // Add animation modifier
                .transition(.move(edge: .trailing)) // Add transition modifier
        
   
    }
    
    static let emptyScrollToString = "Empty"
    
    
    private var messageView_new : some View {
        VStack {
            ScrollView {
                ScrollViewReader { ScrollViewProxy in
                VStack {
                
                    ForEach(vm.messages , id:\.ID) { message in
                        VStack {
                            if (message.SENDER == vm.currentUser?.EMAIL){
                                HStack{
                                    Spacer()
                                    HStack{
                                        Text(message.MESSAGE  )
                                            .foregroundColor( .white)
                                    }
                                    .padding()
                                    .background(Color.blue)
                                    .cornerRadius(8)
                                }
                            } else {
                                HStack{
                                    HStack{
                                        Text(message.MESSAGE  )
                                            .foregroundColor( .black)
                                    }
                                    .padding()
                                    .background(Color.white)
                                    .cornerRadius(8)
                                     Spacer()
                                }
                            }
                        }
                        .padding(.horizontal)
                        .padding(.top, 8)
                     }
                    HStack{Spacer()}
                   .id(Self.emptyScrollToString  )
                }
                .onReceive(vm.$count) { _ in
                    withAnimation(.easeOut(duration: 0.5)) {
                        ScrollViewProxy.scrollTo(Self.emptyScrollToString
                                                 , anchor: .bottom)
                    }
                }
              }
            }
            .background(Color(.init(gray: 0.95, alpha: 1 )))
            .safeAreaInset(edge: .bottom) {
                chatBottomBar_new
             .background(Color(.systemBackground).ignoresSafeArea())
            }
        }
    }
    
    private var chatBottomBar_new : some View {
        HStack(spacing: 16) {
            Image(systemName: "plus")
                .font(.system(size: 24))
                .foregroundColor(Color(.darkGray))
            ZStack {
                DescriptionPlaceholder()
                TextEditor(text: self.$vm.chatText)
                    .opacity(vm.chatText.isEmpty ? 0.5 : 1)
            }
            .frame(height: 40)
            Button {
                vm.handleSend_new()
                
            } label: {
                Text("Send")
                    .foregroundColor(.white)
            }
            .padding(.horizontal)
            .padding(.vertical , 8 )
            .background(Color.blue)
            .cornerRadius(4)
            .disabled(vm.chatText.isEmpty)

        }
        .padding(.horizontal)
        .padding(.vertical, 8)
    }
    
}

private struct DescriptionPlaceholder: View {
    var body: some View {
        HStack {
            Text("Description")
                .foregroundColor(Color(.gray))
                .font(.system(size: 17))
                .padding(.leading, 5)
                .padding(.top, -4)
            Spacer()
        }
    }
}

struct ChatLogView_new_Previews: PreviewProvider {
    static var previews: some View {
//        NavigationView{
//            ChatLogView_new(user: nil , currentUser: nil )
//        }
        if #available(iOS 16.0, *) {
            MainMessagesView()
        } else {
            // Fallback on earlier versions
        }
       
    }
}
