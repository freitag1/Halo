//
//  Service.swift
//  Halo
//
//  Created by 김병일 on 2022/10/19.
//

import Alamofire
import Foundation
import UIKit



class Service : NSObject {
    static let shared = Service()
    
    let baseUrl = "http://localhost:5005"
    
    func signUp(  imgData: UIImage? ,  name: String, email: String, password: String, passwordconfirm : String , completion: @escaping (AFResult<Data>) -> ()) {
        let params = ["name": name, "email": email, "password": password , "passwordconfirm": passwordconfirm]
        let url = "\(baseUrl)/auth/register_halo"
//        AF.request(url, method: .post, parameters: params)
//            .validate(statusCode: 200..<300)
//            .responseData { (dataResp) in
//                if let err = dataResp.error {
//                    completion(.failure(err))
//                    return
//                }
//                completion(.success(dataResp.data ?? Data()))
//        }
        
        AF.upload(multipartFormData: {(formData) in
            
            formData.append(Data(name.utf8), withName: name)
            
            for (key, value) in params {
                           if let temp = value as? String {
                               formData.append(temp.data(using: .utf8)!, withName: key)
                           }
                       }
            
            guard let imageData = imgData?.jpegData(compressionQuality: 0.5) else {return}
            
            print(imageData)

            formData.append(imageData, withName: "imageFile", fileName: "imageFile.jpg", mimeType: "image/jpg")
            
        }, to: url).responseJSON { (res) in
            print("Maybe Successfully register profile image")

        }
    }
    

    
    func uplaodPost( comments: String ,  imgData: [UIImage] , completion: @escaping (AFResult<Data>) -> ()) {

          //  let url: String = "http://localhost:5005/view/save_post"

        let url = "\(baseUrl)/view/save_post"

        AF.upload(multipartFormData: { (formData) in

            formData.append(Data(comments.utf8), withName: "comments")

//            guard let imageData = imgData.jpegData(compressionQuality: 0.5) else {return}
//
//            print(imageData)
//
//            formData.append(imageData, withName: "imageFile", fileName: "imageFile.jpg", mimeType: "image/jpg")
            
            for image_ in imgData {
                guard let imageData = image_.jpegData(compressionQuality: 0.5) else {return}
                    formData.append((imageData), withName: "imageFile", fileName: "imageFile.jpg", mimeType: "image/jpg")
     
            }

            print(formData)

        }, to: url).responseJSON { (res) in
            print("Maybe finishing uplaod")

        }

    }
    
//        var list = [UIImage]()
//
//        func uplaodPosts(){
//            if list.isEmpty{
//                return
//            }
//            uplaodPost(comments: [:] ,  image: list.first!)
//        }
//
//        func uplaodPost(comments:  [String : String], image: UIImage) {
//
//            AF.upload(multipartFormData: { multipartFormData in
//                 let imageData = image.jpegData(compressionQuality: 0.5)
//                multipartFormData.append(imageData!, withName: "imageFile", fileName: "imageFile.jpg", mimeType: "image/jpg")
//                for row in comments {
//                    if !row.value.isEmpty{
//                        multipartFormData.append(row.value.data(using: String.Encoding.utf8)!, withName: row.key)
//                    }
//                }
//            },
//                             to: "http://localhost:5005/view/save_post")
//            { (result) in
//                switch result {
//                case .success(let upload):
//                    upload.uploadProgress(closure: { (progress) in
//                        print("Upload Progress: \(progress.fractionCompleted)")
//                    })
//                    upload.responseString {response in
//                        self.list.removeFirst()
//                        self.uploadImages()
//                    }
//                case .failure(let error):
//                    print(error)
//                }
//            }
//        }

    func login(email: String, password: String, completion: @escaping (AFResult<Data>) -> ()) {
        print("Performing login")
        let params = ["email": email, "password": password]
        let url = "\(baseUrl)/auth/login_halo"
        AF.request(url, method: .post, parameters: params)
            .validate(statusCode: 200..<300)
            .responseData { (dataResp) in
                if let err = dataResp.error {
                    completion(.failure(err))
                } else {
                    completion(.success(dataResp.data ?? Data()))
                }
        }
    }
    
    
    func fetchRooms(completion: @escaping (Result<[Room] , Error>) -> ()) {
        
        let url = "\(baseUrl)/rooms"
        AF.request(url)
            .validate(statusCode: 200..<300)
            .responseData { (dataResp) in
                if let err = dataResp.error {
                    completion(.failure(err))
                    return
                }
                
                guard let data = dataResp.data else { return }
                do {
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .formatted(dateFormatter)
                    
                    let rooms = try decoder.decode([Room].self, from: data)
                    completion(.success(rooms))
                } catch {
                    completion(.failure(error))
                }
        }
        
//        let urladress = "http://localhost:5005/posts"
//
//        guard let url = URL(string: urladress ) else {return}
//
//        URLSession.shared.dataTask(with: url) { (data, resp, err) in
//
//            DispatchQueue.main.async {
//                if let err = err {
//                    print("Failed to fetch posts")
//                    return
//                }
//                guard let data = data else {
//                    return
//                }
//
//                do {
//                    let posts = try JSONDecoder().decode([Post].self, from: data)
//                    completion(.success(posts))
//                    //print(String(data: data, encoding: .utf8) ?? "")
//                } catch {
//                    completion(.failure(error))
//                }
//            }
//
//        }.resume()
        
    }
    
    

    
    func fetchPosts(completion: @escaping (Result<[Post] , Error>) -> ()) {
        
        let url = "\(baseUrl)/posts"
        AF.request(url)
            .validate(statusCode: 200..<300)
            .responseData { (dataResp) in
                if let err = dataResp.error {
                    completion(.failure(err))
                    return
                }
                
                guard let data = dataResp.data else { return }
                do {
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .formatted(dateFormatter)
                    
                    let posts = try decoder.decode([Post].self, from: data)
                    completion(.success(posts))
                } catch {
                    completion(.failure(error))
                }
                
                
        }
        
//        let urladress = "http://localhost:5005/posts"
//
//        guard let url = URL(string: urladress ) else {return}
//
//        URLSession.shared.dataTask(with: url) { (data, resp, err) in
//
//            DispatchQueue.main.async {
//                if let err = err {
//                    print("Failed to fetch posts")
//                    return
//                }
//                guard let data = data else {
//                    return
//                }
//
//                do {
//                    let posts = try JSONDecoder().decode([Post].self, from: data)
//                    completion(.success(posts))
//                    //print(String(data: data, encoding: .utf8) ?? "")
//                } catch {
//                    completion(.failure(error))
//                }
//            }
//
//        }.resume()
        
    }
    
    func fetchCurrentUser_nodejs(completion: @escaping (Result<User, Error>) -> ()) {
        
        let url = "\(baseUrl)/currentuser"
        AF.request(url)
            .validate(statusCode: 200..<300)
            .responseData { (dataResp) in
                if let err = dataResp.error {
                    completion(.failure(err))
                    return
                }
                
                guard let data = dataResp.data else { return }
                do {
                    let users = try JSONDecoder().decode(User.self, from: data)
                    completion(.success(users))
                } catch {
                    completion(.failure(error))
                }
        }

    }
    
    func fetchMessages(fromId: String, toId: String, roomId : String , completion: @escaping (Result<[Message] , Error>) -> ()) {
        let params = ["fromId": fromId, "toId": toId , "roomId" : roomId]
        let url = "\(baseUrl)/view/messages"
        AF.request(url, method: .post, parameters: params)
            .validate(statusCode: 200..<300)
            .responseData { (dataResp) in
                if let err = dataResp.error {
                    completion(.failure(err))
                    return
                }
                
                guard let data = dataResp.data else { return }
                do {
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .formatted(dateFormatter)
                    
                  //  let posts = try decoder.decode([Post].self, from: data)
                    
                    let messages = try decoder.decode([Message].self, from: data)
                    completion(.success(messages))
                } catch {
                    completion(.failure(error))
                }
            }
    }

    
    
    func saveMessage(fromId: String, toId: String, chatText: String, roomId: String , completion: @escaping (Result<[Message] , Error>) -> ()) {
        let params = ["fromId": fromId, "toId": toId, "chatText": chatText, "roomId": roomId]
        let url = "\(baseUrl)/view/save_message"
        AF.request(url, method: .post, parameters: params)
            .validate(statusCode: 200..<300)
            .responseData { (dataResp) in
                if let err = dataResp.error {
                    completion(.failure(err))
                    return
                }
                
                guard let data = dataResp.data else { return }
                do {
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .formatted(dateFormatter)
                    
                  //  let posts = try decoder.decode([Post].self, from: data)
                    
                    let messages = try decoder.decode([Message].self, from: data)
                    completion(.success(messages))
                } catch {
                    completion(.failure(error))
                }
            }
    }
    
    func fetchFriendUsers_nodejs(completion: @escaping (Result<[User] , Error>) -> ()) {
        
        let url = "\(baseUrl)/frienduser"
        AF.request(url)
            .validate(statusCode: 200..<300)
            .responseData { (dataResp) in
                if let err = dataResp.error {
                    completion(.failure(err))
                    return
                }
                
                guard let data = dataResp.data else { return }
                do {
                    let friendusers = try JSONDecoder().decode([User].self, from: data)
                    completion(.success(friendusers))
                } catch {
                    completion(.failure(error))
                }
            }
    }
    
   
    
    
}
