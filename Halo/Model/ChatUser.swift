//
//  ChatUser.swift
//  Halo
//
//  Created by 김병일 on 2022/07/09.
//

import FirebaseFirestoreSwift

struct ChatUser: Codable, Identifiable {
    @DocumentID var id: String?
    let uid, email, profileImageUrl: String
}

 
