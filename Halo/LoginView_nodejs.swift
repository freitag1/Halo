////
////  LoginView_nodejs.swift
////  Halo
////
////  Created by 김병일 on 2022/07/30.
////
//
//import SwiftUI
//import Alamofire
//
//struct  LoginView_nodejs: View {
//     
//    let didCompleteLoginProcess : () -> ()
//    
//    @State private var isLoginMode = false
//    @State private var email = ""
//    @State private var password = ""
//    @State private var name = ""
//    @State private var passwordConfirm = ""
//    @State private var company = ""
//    
//    @State private var shoudShowImagePicker = false
//    @State var shoudShowMainMessageScreen = false
//
//    var body: some View {
//        NavigationView {
//            ScrollView {
//                  
//                VStack(spacing : 16  ){
//                    
//                    Picker(selection: $isLoginMode  , label:
//                            Text("Picker here")) {
//                        Text("Log in").tag(true)
//                        Text("Create Account").tag(false)
//                    }.pickerStyle(SegmentedPickerStyle() )
//                    
//                    if !isLoginMode {
//                        //회원가입
//                        Button {
//                            shoudShowImagePicker.toggle()
//                        } label: {
//                            
//                            VStack{
//                                
//                                if let image = self.image {
//                                    Image(uiImage: image)
//                                        .resizable()
//                                        .scaledToFill()
//                                        .frame(width: 128, height: 128)
//                                        .cornerRadius(64)
//                                } else {
//                                    
//                                    Image(systemName: "person.fill").font(.system(size: 64)).padding().foregroundColor(Color(.label))
//                                }
//                            }
//                            .overlay(RoundedRectangle(cornerRadius: 64).stroke(Color.black , lineWidth: 3))
//                            
//                        }
//                        Group {
//                            TextField("Name", text: $name)
//                              TextField("Email", text: $email).keyboardType(.emailAddress).autocapitalization(.none)
//                              SecureField("Password", text: $password)
//                            SecureField("Passwordconfirm", text: $passwordConfirm)
//                            TextField("Company", text: $company)
//                        }.padding()
//                            .background(Color.white)
//                        
//                    }
//                    
//                    else {
//                    
//                            Group {
//                                  TextField("Email", text: $email).keyboardType(.emailAddress).autocapitalization(.none)
//                                  SecureField("Password", text: $password)
//                            }.padding()
//                                .background(Color.white)
//                    }
//                  
//                    Button {
//                        handleAction()
//                    } label: {
//                        HStack{
//                            Spacer()
//                            Text(isLoginMode ? "Log in" : "Create Account")
//                                .foregroundColor(.white)
//                                .padding(.vertical,10)
//                                .font(.system(size: 14, weight: .semibold))
//                            Spacer()
//                        }.background(Color.blue)
//                    }
//                    .fullScreenCover(isPresented: $shoudShowMainMessageScreen){
//                        MainMessagesView_nodejs()
//                    }
//                    Text(self.loginStatusMessage).foregroundColor(.red)
//                    
//                }.padding()
//                
//            }
//            .navigationTitle(isLoginMode ? "Log in" : "Create Account")
//            .background(Color(.init(white: 0, alpha: 0.05)).ignoresSafeArea() )
//        }
//        .navigationViewStyle(StackNavigationViewStyle())
//        .fullScreenCover(isPresented:   $shoudShowImagePicker , onDismiss: nil) {
//            ImagePicker(image : $image)
//        }
//    }
//    
//    @State var image: UIImage?
//    
//    
//    private func handleAction() {
//           if isLoginMode {
//              // print("Should log into Firebase with e  xisting credentials")
//               
//               loginUser()
//           } else {
//               
//               createNewAccount()
//               print("Register a new account inside of Firebase Auth and then store image in Storage somehow....")
//           }
//       }
//    
//    private func loginUser(){
//        
//        //firebase
//        
////        FirebaseManager.shared.auth.signIn(withEmail: email, password: password){
////            result, error in
////            if let err = error {
////                print("Failed to login user" , err)
////                self.loginStatusMessage = "Failed to login user \(err)"
////                return
////            }
////
////            print("Successfully login user: \(result?.user.uid ?? "")")
////
////            self.loginStatusMessage = "Successfully login user: \(result?.user.uid ?? "")"
////
////            //self.didCompleteLoginProcess()
////
////            shoudShowMainMessageScreen.toggle()
////
////        }
//        
//        //nodejs connect
//        
////        guard let url = URL(string: "http://localhost:5003/auth/login") else {return}
////
////        print(" 1111")
////
////        var loginRequest = URLRequest(url: url)
////        loginRequest.httpMethod = "POST"
////
////        print(" 2222")
////
////        do {
////            let params = [ "email" : email  , "password:" : password   ]
////            loginRequest.httpBody = try JSONSerialization.data(withJSONObject: params, options: .init())
////
////            print(params)
////
////            URLSession.shared.dataTask(with: loginRequest) { (data , resp , err) in
////                if let err = err {
////
////                    print(" 44444")
////                    print("Failed to login nodejs server" , err)
////                    return
////                }
////
////                print(" resp : " , resp)
////
////                print(" probably Success login  ")
////
////            }.resume() //never forget this resume
////
////        } catch {
////            print("Failed to serialize data" , error)
////        }
//        
//        let params = [ "email" : email  , "password" : password   ]
//        
//        guard let url = URL(string: "http://localhost:5005/auth/login") else {
//            return    // 데이터를 보낼 서버 url
//        }
//                        
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"         // http 메서드는 'POST'
//                
//        do { // request body에 전송할 데이터 넣기
//            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
//        } catch {
//            print(error.localizedDescription)
//        }
//                
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.addValue("application/json", forHTTPHeaderField: "Accept-Type")
//        
//        let session = URLSession.shared
//        session.dataTask(with: request, completionHandler: { (data, resp, err) in
//                if let err = err {
//
//                    print(" 44444")
//                    print("Failed to login nodejs server" , err)
//                    return
//                }
//
//                print(" resp : " , resp)
//
//                print(" probably Success login  ")
//            
//            self.didCompleteLoginProcess()
//            
//            shoudShowMainMessageScreen.toggle()
//            
//        }).resume()
//        
//        
//        
//        
//    }
//    
//    @State var loginStatusMessage = ""
//    
//    private func createNewAccount() {
////        if self.image == nil {
////            self.loginStatusMessage = "You must select avatar image"
////            return
////        }
//        
//        //firebase version
//        
////        FirebaseManager.shared.auth.createUser(withEmail: email, password: password) {
////            result, error in
////            if let err = error {
////                print("Failed to create user" , err)
////                self.loginStatusMessage = "Failed to create user \(err)"
////                return
////            }
////
////            print("Successfully create user: \(result?.user.uid ?? "")")
////
////            self.loginStatusMessage = "Successfully create user: \(result?.user.uid ?? "")"
////
////            self.persistImageToStorage()
////        }
//        
//        //nodejs version
//        
//        let params = [ "name" : name ,  "email" : email  ,
//                       "password" : password , "passwordConfirm" : passwordConfirm ,
//                       "company" : company ]
//        
//        guard let url = URL(string: "http://localhost:5003/auth/register") else {
//            return    // 데이터를 보낼 서버 url
//        }
//                        
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"         // http 메서드는 'POST'
//                
//        do { // request body에 전송할 데이터 넣기
//            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
//        } catch {
//            print(error.localizedDescription)
//        }
//                
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.addValue("application/json", forHTTPHeaderField: "Accept-Type")
//        
//        let session = URLSession.shared
//        session.dataTask(with: request, completionHandler: { (data, resp, err) in
//                if let err = err {
//
//                    print(" 44444")
//                    print("Failed to register nodejs server" , err)
//                    return
//                }
//
//                print(" resp : " , resp)
//
//                print(" probably Success register user  ")
//            
//           // shoudShowMainMessageScreen.toggle()
//            
//        }).resume()
//        
//        
//    }
//    
////    private func persistImageToStorage() {
//////        let filename = UUID().uuidString
////        guard let uid = FirebaseManager.shared.auth.currentUser?.uid else { return }
////        let ref = FirebaseManager.shared.storage.reference(withPath: uid)
////        guard let imageData = self.image?.jpegData(compressionQuality: 0.5) else { return }
////        ref.putData(imageData, metadata: nil) { metadata, err in
////            if let err = err {
////                self.loginStatusMessage = "Failed to push image to Storage: \(err)"
////                return
////            }
////
////            ref.downloadURL { url, err in
////                if let err = err {
////                    self.loginStatusMessage = "Failed to retrieve downloadURL: \(err)"
////                    return
////                }
////
////                self.loginStatusMessage = "Successfully stored image with url: \(url?.absoluteString ?? "")"
////                print(url?.absoluteString)
////
////                guard let url = url else {return}
////                self.storeUserInformation(imageProfileUrl: url)
////            }
////        }
////    }
//    
////    private func storeUserInformation(imageProfileUrl: URL) {
////        guard let uid = FirebaseManager.shared.auth.currentUser?.uid else { return }
////        let userData = ["email": self.email, "uid": uid, "profileImageUrl": imageProfileUrl.absoluteString]
////        FirebaseManager.shared.firestore.collection("users")
////            .document(uid).setData(userData) { err in
////                if let err = err {
////                    print(err)
////                    self.loginStatusMessage = "\(err)"
////                    return
////                }
////
////                print("Success")
////
////                self.didCompleteLoginProcess()
////            }
////    }
//    
//    
//}
//
//struct ContentView_Previews1: PreviewProvider {
//    static var previews: some View {
//        LoginView_nodejs(didCompleteLoginProcess: {
//            
//        })
//    }
//}
