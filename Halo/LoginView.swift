//
//  ContentView.swift
//  Halo
//
//  Created by 김병일 on 2022/05/14.
//

import SwiftUI
import Alamofire
import JGProgressHUD_SwiftUI

struct  LoginView: View {
     
    let didCompleteLoginProcess : () -> ()
    
    @State private var isLoginMode = false
    @State private var name = ""
    @State private var email = ""
    @State private var password = ""
    @State private var passwordconfirm = ""
    
    @State private var shoudShowImagePicker = false
   // @State var shoudShowMainMessageScreen = false
    @State var shoudShowHaloTabbarScreen = false
    
    @State private var isLoading = false
    @State private var isMaskingEnabled = true

    var body: some View {
        NavigationView {
            ScrollView {
                VStack(spacing : 16  ){
                    Picker(selection: $isLoginMode  , label:
                            Text("Picker here")) {
                        Text("Log In").tag(true)
                        Text("Create Account").tag(false)
                    }.pickerStyle(SegmentedPickerStyle() )
                    if !isLoginMode {  //회원가입
                                          Button {
                                              shoudShowImagePicker.toggle()
                                          } label: {
                                              VStack{
                                                  if let image = self.image {
                                                      Image(uiImage: image)
                                                          .resizable()
                                                          .scaledToFill()
                                                          .frame(width: 128, height: 128)
                                                          .cornerRadius(64)
                                                  } else {
                                                      Image(systemName: "person.fill").font(.system(size: 64)).padding().foregroundColor(Color(.label))
                                                  }
                                              }
                                              .overlay(RoundedRectangle(cornerRadius: 64).stroke(Color.black , lineWidth: 3))
                                          }
                                          Group {
                                              TextField("Name", text: $name)
                                                TextField("Email", text: $email).keyboardType(.emailAddress).autocapitalization(.none)
                                                SecureField("Password", text: $password)
                                              SecureField("Passwordconfirm", text: $passwordconfirm)
                                          }.padding()
                                              .background(Color.white)
                                      }
                                      else {
                                              Group {
                                                    TextField("Email", text: $email).keyboardType(.emailAddress).autocapitalization(.none)
                                                    SecureField("Password", text: $password)
                                                  
                                              }.padding()
                                                  .background(Color.white)
                                      }
                    Button {
                        handleAction()
                        
                       
                    } label: {
                        HStack{
                            Spacer()
                            Text(isLoginMode ? "Log In" : "Create Account")
                                .foregroundColor(.white)
                                .padding(.vertical,10)
                                .font(.system(size: 14, weight: .semibold))
                            Spacer()
                        }.background(Color.blue)
                    }
                    Text(self.loginStatusMessage).foregroundColor(.red)
                    Toggle("Disable Masking", isOn: $isMaskingEnabled)
                                  .padding(.top, 20)
                }.padding()
                
            }
            .navigationTitle(isLoginMode ? "Log In" : "Create Account")
            .background(Color(.init(white: 0, alpha: 0.05)).ignoresSafeArea() )

        }
        .navigationViewStyle(StackNavigationViewStyle())
        .fullScreenCover(isPresented:   $shoudShowImagePicker , onDismiss: nil) {
            ImagePicker(image : $image)
        }
        
    }
    
    @State var image: UIImage?
    
    private func handleAction() {
           if isLoginMode {
              // print("Should log into Firebase with e  xisting credentials")
               
              // loginUser()  // firebase
      
               loginUser_nodejs()
               
               
           } else {
               
               // createNewAccount()  //firebase
               
               createNewAccount_nodejs() // nodejs
               print("Register a new account inside of Firebase Auth and then store image in Storage somehow....")
           }
       }
    
    private func loginUser(){
        
        //firebase
        
        FirebaseManager.shared.auth.signIn(withEmail: email, password: password){
            result, error in
            if let err = error {
                print("Failed to login user" , err)
                self.loginStatusMessage = "Failed to login user \(err)"
                return
            }

            print("Successfully login user: \(result?.user.uid ?? "")")

            self.loginStatusMessage = "Successfully login user: \(result?.user.uid ?? "")"

            //self.didCompleteLoginProcess()

      //      shoudShowHaloTabbarScreen.toggle()
            
        //    shoudShowMainMessageScreen.toggle()

        }
        
    }
    
    private func loginUser_nodejs(){
        
//        let hud = JGProgressHUD()
//        hud.textLabel.text = "Logging in"
//        hud.dismiss(afterDelay: 3)
        
        Service.shared.login(email: email, password: password) { (res) in
            switch res {
            case .failure(let err):
                print("Failed to login " , err)
            case .success:
                print("Log in Successfully")
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                  // 1초 후 실행될 부분
                    
                    self.didCompleteLoginProcess()
                }
                
            }
        }
        
        SocketIOManager.shared.joinRoom_new(roomid: email, sender: email);

        
    }
    
    @State var loginStatusMessage = ""
    
    private func createNewAccount() {
        if self.image == nil {
            self.loginStatusMessage = "You must select avatar image"
            return
        }
        
        FirebaseManager.shared.auth.createUser(withEmail: email, password: password) {
            result, error in
            if let err = error {
                print("Failed to create user" , err)
                self.loginStatusMessage = "Failed to create user \(err)"
                return
            }
            
            print("Successfully create user: \(result?.user.uid ?? "")")
            
            self.loginStatusMessage = "Successfully create user: \(result?.user.uid ?? "")"
            
            self.persistImageToStorage()
        }
    }
    
    
    @State private var showingAlert = false
    
    private func createNewAccount_nodejs() {
        if self.image == nil {
            self.loginStatusMessage = "You must select avatar image"
            return
        }
        print("image");
        print(self.image);
        
        guard let imageData = self.image?.jpegData(compressionQuality: 0.5) else { return }
        
        Service.shared.signUp(imgData: self.image ,  name: name, email: email, password: password ,
                              passwordconfirm: passwordconfirm) { (res) in
        switch res {
        case .failure(let err):
            print("Failed to sign up:", err)
        case .success:
            print("Successfully signed up")
            
        }
        }
    }
    
    private func persistImageToStorage() {
//        let filename = UUID().uuidString
        guard let uid = FirebaseManager.shared.auth.currentUser?.uid else { return }
        let ref = FirebaseManager.shared.storage.reference(withPath: uid)
        guard let imageData = self.image?.jpegData(compressionQuality: 0.5) else { return }
        ref.putData(imageData, metadata: nil) { metadata, err in
            if let err = err {
                self.loginStatusMessage = "Failed to push image to Storage: \(err)"
                return
            }
            
            ref.downloadURL { url, err in
                if let err = err {
                    self.loginStatusMessage = "Failed to retrieve downloadURL: \(err)"
                    return
                }
                
                self.loginStatusMessage = "Successfully stored image with url: \(url?.absoluteString ?? "")"
                print(url?.absoluteString)
                
                guard let url = url else {return}
                self.storeUserInformation(imageProfileUrl: url)
            }
        }
    }
    
    private func storeUserInformation(imageProfileUrl: URL) {
        guard let uid = FirebaseManager.shared.auth.currentUser?.uid else { return }
        let userData = ["email": self.email, "uid": uid, "profileImageUrl": imageProfileUrl.absoluteString]
        FirebaseManager.shared.firestore.collection("users")
            .document(uid).setData(userData) { err in
                if let err = err {
                    print(err)
                    self.loginStatusMessage = "\(err)"
                    return
                }

                print("Success")
                
                self.didCompleteLoginProcess()
            }
    }
    
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView(didCompleteLoginProcess: {
            
        })
    }
}
