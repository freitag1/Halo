//
//  MainMessagesView.swift
//  Halo
//
//  Created by 김병일 on 2022/06/25.
//

import PhotosUI
import SwiftUI
import SDWebImageSwiftUI
import Firebase
import FirebaseFirestoreSwift
import Alamofire
import SocketIO


struct User: Codable  {
    let ID : Int
    let EMAIL  , NAME , PROFILEIMAGE , ROOMID : String
}


class  MainMessagesViewModel: ObservableObject {

    @Published var errorMessage = ""
    @Published var chatUser : ChatUser?
    @Published var currentUser : User?
    
    @Published var isUserCurrentlyLoggedOut = false

    init() {

        DispatchQueue.main.async {
            self.isUserCurrentlyLoggedOut = self.currentUser == nil
        }
        receivedMessage()
  
        fetchCurrentUser_nodejs()
        
       // fetchPosts()
        fetchRooms()
        
    }
	
    @Published var recentMessages = [RecentMessage]()
    @Published var posts = [Post]()  // post test
    @Published var user = [User]()  // post test
    @Published var rooms = [Room]()
    
    private var firestoreListener: ListenerRegistration?
    
    func receivedMessage() {
        
        //message received
        
        let socket = SocketIOManager.shared.manager.socket(forNamespace: "/")
        
                socket.off("message")
                socket.on("message") { (data, ack) in

                print("main Message View")
                print(data)

                let data = data[0] as! NSDictionary
                let sender = data["SENDER"] as! String
                let message = data["MESSAGE"] as! String
                let roomid = data["ROOMID"] as! String
                }
    }
    
    
    
    
    fileprivate func fetchPosts() {
        Service.shared.fetchPosts { (res) in
            switch res {
            case .failure(let err):
                print("Failed to fetch post" , err)
            case .success(let posts):
                print("posts in mainMessage")
                // print(posts)
                self.posts = posts
            }
        }
    }
    
    fileprivate func fetchRooms() {
        Service.shared.fetchRooms { (res) in
            switch res {
            case .failure(let err):
                print("Failed to fetch room" , err)
            case .success(let rooms):
                print("rooms in mainMessage")
                 print(rooms)
                self.rooms = rooms
            }
        }
    }

     func fetchCurrentUser_nodejs() {
        Service.shared.fetchCurrentUser_nodejs { (res) in
            switch res {
            case .failure(let err):
                print("Failed to get userinfo" , err)
            case .success(let user):
                print("currentuser in mainMessage")
                print(user.EMAIL)
                self.currentUser = user
                
                SocketIOManager.shared.joinRoom_new(roomid: user.ROOMID, sender: user.EMAIL)
            }
        }
    }
 
 
    func handleSignOut_nodejs() {
         isUserCurrentlyLoggedOut.toggle()
        guard let url = URL(string: "http://localhost:5005/auth/logout_halo") else {
                 return    // 데이터를 보낼 서버 url
             }
             var request = URLRequest(url: url)
             request.httpMethod = "GET"         // http 메서드는 'POST'
             let session = URLSession.shared
             session.dataTask(with: request, completionHandler: { (data, resp, err) in
                     if let err = err {
                         //print(" 44444")
                         print("Failed to logout nodejs server" , err)
                         return
                     }
                    // print(" resp : " , resp)
                     print(" probably Success logout  ")
                 
             }).resume()
    }
    
    
}



@available(iOS 16.0, *)
struct MainMessagesView: View {

    @State var shouldShowLogOutOptions = false
    @State var shoudNavigateChatLogView = false  //single chat log view
    @State var shoudShowModel = false  //create Feed View
    @ObservedObject private var vm = MainMessagesViewModel()
    
    @State var selectedIndex = 0
    @State var image: UIImage?
   
    
    let mainTabBarImageNames = ["person" , "message" , "ellipsis"]
    let tabBarImageNames = ["message" , "plus.app.fill" , "photo"]

    var body: some View {
        NavigationView {
            VStack {
                    customNavBar
                    mainTabbarView
                    navigationView
            }
//            .overlay(
//                newMessageButton, alignment: .bottom)
            .navigationBarHidden(true)
        }
    }
    
    private var customNavBar: some View {
        HStack(spacing: 16) {
            WebImage(url: URL(string: vm.currentUser?.PROFILEIMAGE ?? "" ))
                .resizable()
                .scaledToFill()
                .frame(width: 50, height: 50)
                .clipped()
                .cornerRadius(50)
                .overlay(RoundedRectangle(cornerRadius: 44)
                            .stroke(Color(.label), lineWidth: 1)
                )
                .shadow(radius: 5  )
            VStack(alignment: .leading, spacing: 4) {
                Text(vm.currentUser?.NAME ?? "")
                    .font(.system(size: 24, weight: .bold))
                HStack {
                    Circle()
                        .foregroundColor(.green)
                        .frame(width: 14, height: 14)
    	                Text("Online")
                        .font(.system(size: 12))
                        .foregroundColor(Color(.lightGray))
                }
            }
            Spacer()
            Button {
                shouldShowLogOutOptions.toggle()
            } label: {
                Image(systemName: "gear")
                    .font(.system(size: 24, weight: .bold))
                    .foregroundColor(Color(.label))
            }
        }
        .padding()
        .actionSheet(isPresented: $shouldShowLogOutOptions) {
            .init(title: Text("Settings"), message: Text("What do you want to do?"), buttons: [
                .destructive(Text("Sign Out"), action: {
                    print("handle sign out")

                   // vm.handleSignOut()
                    vm.handleSignOut_nodejs()
                }),
                    .cancel()
            ])
        }
        .fullScreenCover(isPresented: $vm.isUserCurrentlyLoggedOut, onDismiss: nil){
            LoginView(didCompleteLoginProcess: {
                self.vm.isUserCurrentlyLoggedOut = false
                self.vm.fetchCurrentUser_nodejs()
//                self.vm.fetchCurrentUser()
//                self.vm.fetchRecentMessages()
            })
        }
    }
    
    private var chatLogView_new_model = ChatLogView_new_model(user: nil, currentUser: nil)

    private var navigationView: some View {
        NavigationLink( "", isActive: $shoudNavigateChatLogView) {
            VStack(alignment: .leading) {
                ZStack {
                    Spacer().sheet(isPresented: $shoudShowModel) {
                        CreateFeedView()
                    }
                    switch selectedIndex {
                    case 0 :
                       // ChatLogView(vm: chatLogViewModel  )
//                        ChatLogView_new(user: self.user , currentUser: self.currentuser)
                        ChatLogView_new(vm: chatLogView_new_model)
                        //TestView(user: self.user)
                    case 2 :
                        FeedLogView(user: self.user)
                    default :
                       // ChatLogView(vm: chatLogViewModel  )
//                        ChatLogView_new(user: self.user , currentUser: self.currentuser)
                        ChatLogView_new(vm: chatLogView_new_model)
                       // TestView(user: self.user)
                    }
                }
                Spacer()
                HStack{
                    ForEach(0..<3) { num in
                        Button(action: {
                            if num == 1 {
                                shoudShowModel.toggle()
                                return
                            }
                            selectedIndex = num }, label:
                                {
                            Spacer()
                            if num == 1 {

                                    Image(systemName: tabBarImageNames[num])
                                        .font(.system(size: 44 , weight: .bold))
                                        .foregroundColor(.red)
                                Spacer()
                            }
                            else
                            {        Image(systemName: tabBarImageNames[num])
                                    .font(.system(size: 24 , weight: .bold))
                                    .foregroundColor(selectedIndex == num ? Color(.black) : .init(white: 0.8))
                                Spacer()}
                        })
                    }
                }
            }
        }
    }
    
    private var mainTabbarView: some View {
        VStack {
            ZStack {
                switch selectedIndex {
                case 0 :
                    friendListView
                case 1 :
                    roomView
                case 2 :
                    friendListView
                default :
                    friendListView
                }
            }
            Spacer()
            HStack{
                ForEach(0..<3) { num in
                    Button(action: {
                        selectedIndex = num }, label:
                            {
                        Spacer()
                        Image(systemName: mainTabBarImageNames[num])
                        .font(.system(size: 24 , weight: .bold))
                        .foregroundColor(selectedIndex == num ? Color(.black) : .init(white: 0.8))
                            Spacer()
                    })
                }
            }
        }
    }
    
    private var postView: some View {
        ScrollView {
            ForEach(vm.posts , id: \.POST_ID) { post in
                VStack {
                    HStack(spacing: 16) {
                        WebImage(url: URL(string: post.PROFILEIMAGE ?? ""  ))
                            .resizable()
                            .scaledToFill()
                            .frame(width: 64, height: 64)
                            .clipped()
                            .cornerRadius(64)
                            .overlay(RoundedRectangle(cornerRadius: 64)
                                        .stroke(Color(.label), lineWidth: 1)
                            )
                            .shadow(radius: 5  )
                        VStack(alignment: .leading) {
                            Text(post.NAME)
                                .font(.system(size: 16, weight: .bold))
                            Text(post.COMMENTS)
                                .font(.system(size: 14))
                                .foregroundColor(Color(.lightGray))
                        }
                        Spacer()
                        Text(post.timeAgo)
                            .font(.system(size: 14, weight: .semibold))
                    }
                    Divider()
                        .padding(.vertical, 8)
                }.padding(.horizontal)
                
            }.padding(.bottom, 50)
        }
    }
    
    private var roomView: some View {
        ScrollView {
            ForEach(vm.rooms , id: \.ID) { room in
                VStack {
                    HStack(spacing: 16) {
                        WebImage(url: URL(string: room.ROOMID ?? ""  ))
                            .resizable()
                            .scaledToFill()
                            .frame(width: 64, height: 64)
                            .clipped()
                            .cornerRadius(64)
                            .overlay(RoundedRectangle(cornerRadius: 64)
                                        .stroke(Color(.label), lineWidth: 1)
                            )
                            .shadow(radius: 5  )
                        VStack(alignment: .leading) {
                            Text(room.ROOMUSERNAME)
                                .font(.system(size: 16, weight: .bold))
                            Text(room.RESENTMESSAGE)
                                .font(.system(size: 14))
                                .foregroundColor(Color(.lightGray))
                        }
                        Spacer()
                        Text(room.ROOMID)
                            .font(.system(size: 14, weight: .semibold))
                    }
                    Divider()
                        .padding(.vertical, 8)
                }.padding(.horizontal)
                
            }.padding(.bottom, 50)
        }
    }




    private var messagesView: some View {
        ScrollView {
            ForEach(vm.recentMessages) { recentMessage in
                VStack {
                    Button {
//                        let uid = FirebaseManager.shared.auth.currentUser?.uid == recentMessage.fromId ? recentMessage.toId : recentMessage.fromId
//                        self.chatUser = .init(id: uid, uid: uid, email: recentMessage.email, profileImageUrl: recentMessage.profileImageUrl)
//                        self.chatLogViewModel.chatUser = self.chatUser
//                        self.chatLogViewModel.fetchMessages()
//                        self.shoudNavigateChatLogView.toggle()
                    } label: {
                        HStack(spacing: 16) {
                            WebImage(url: URL(string: recentMessage.profileImageUrl))
                                .resizable()
                                .scaledToFill()
                                .frame(width: 64, height: 64)
                                .clipped()
                                .cornerRadius(64)
                                .overlay(RoundedRectangle(cornerRadius: 64)
                                            .stroke(Color.black, lineWidth: 1))
                                .shadow(radius: 5)
                            VStack(alignment: .leading, spacing: 8) {
                                Text(recentMessage.username)
                                    .font(.system(size: 16, weight: .bold))
                                    .foregroundColor(Color(.label))
                                    .multilineTextAlignment(.leading)
                                Text(recentMessage.text)
                                    .font(.system(size: 14))
                                    .foregroundColor(Color(.darkGray))
                                    .multilineTextAlignment(.leading)
                            }
                            Spacer()

                            Text(recentMessage.timeAgo)
                                .font(.system(size: 14, weight: .semibold))
                                .foregroundColor(Color(.label))
                        }
                    }
                    Divider()
                    .padding(.vertical, 8)
                }.padding(.horizontal)

            }.padding(.bottom, 50)
        }
    }

    @State var shouldShowNewMassegeScreen = false

    private var newMessageButton: some View {
     
        Button {

            shouldShowNewMassegeScreen.toggle()

        } label: {
            HStack {
                Spacer()
                Text("+ New Message")
                    .font(.system(size: 16, weight: .bold))
                Spacer()
            }
            .foregroundColor(.white)
            .padding(.vertical)
                .background(Color.blue)
                .cornerRadius(32)
                .padding(.horizontal)
                .shadow(radius: 15)
        }
//        .fullScreenCover(isPresented: $shouldShowNewMassegeScreen){
//            CreateNewMessageView(didSelectNewUser: {
//
//                user in print(user.EMAIL)
//               // chatUser in print(vm.currentUser?.EMAIL)
//    //                SocketIOManager.shared.establishConnection()
//                SocketIOManager.shared.chat_login(id: vm.currentUser!.EMAIL ?? "")
//                self.shoudNavigateChatLogView.toggle()  // go to the selected users chat log view
//                self.user = user  //toId
//                self.currentuser = vm.currentUser //fromId
//                //방 만들기 요청
//
//            } )
//        }
        
  
    }
    
    private var friendListView : some View {
        FriendView(didSelectNewUser: {
            
            user in print(user.EMAIL)
           // chatUser in print(vm.currentUser?.EMAIL)
            SocketIOManager.shared.establishConnection()
           // chatLogView_new_model.receivedMessage()
            //SocketIOManager.shared.chat_login(id: vm.currentUser?.EMAIL ?? "")
            self.shoudNavigateChatLogView.toggle()  // go to the selected users chat log view
            self.user = user  //toId
            self.currentuser = vm.currentUser //fromId
            self.chatLogView_new_model.user           = user
            self.chatLogView_new_model.currentUser = self.vm.currentUser
            self.chatLogView_new_model.fetchMessages()
            self.vm.fetchCurrentUser_nodejs()
            SocketIOManager.shared.joinRoom_new(roomid: user.ROOMID, sender: vm.currentUser?.EMAIL ?? "")

            //방 만들기 요청

        } )
    }
    


   // @State var chatUser : ChatUser? //firebaseㄱ
    @State var user : User?
    @State var currentuser : User?
}

struct MainMessagesView_Previews: PreviewProvider {
    static var previews: some View {
        if #available(iOS 16.0, *) {
            MainMessagesView()
        } else {
            // Fallback on earlier versions
        }
    }
}

