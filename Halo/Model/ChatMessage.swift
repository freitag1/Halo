//
//  ChatMessage.swift
//  Halo
//
//  Created by 김병일 on 2022/09/02.
//

//import Foundation
//import FirebaseFirestoreSwift
//
//struct ChatMessage :Codable , Identifiable {
//
//    var id : String {documentId  }
//
//    let documentId : String
//    let fromID , toId , text : String
//
//    init(documentId : String ,  data: [String: Any]) {
//
//        self.documentId = documentId
//        self.fromID = data[FirebaseConstants.fromId] as? String ?? ""
//        self.toId = data[FirebaseConstants.toId] as? String ?? ""
//        self.text = data[FirebaseConstants.text  ] as? String ?? ""
//    }
//}

import Foundation
import FirebaseFirestoreSwift

struct ChatMessage: Codable, Identifiable {
    @DocumentID var id: String?
    let fromId, toId, text: String
    let timestamp: Date
}

