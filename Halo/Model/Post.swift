//
//  Post.swift
//  Halo
//
//  Created by 김병일 on 2022/12/22.
//

import Foundation

struct Post: Codable  {
    let POST_ID : String
    let EMAIL , COMMENTS ,  NAME ,  PROFILEIMAGE: String
    let INITDTTM : Date
    
    var timeAgo: String {
        let formatter = RelativeDateTimeFormatter()
        formatter.unitsStyle = .abbreviated
        return formatter.localizedString(for: INITDTTM, relativeTo: Date())
    }
}

struct Posts: Codable, Identifiable {
    let post: PostInfo
  //  let comments: [Comment]
    let images: [ImageInfo]?
    
    var id: Int { post.POST_ID }
}

struct PostInfo: Codable {
    let POST_ID: Int
    let NAME: String
    let COMMENTS: String
    let EMAIL: String
    let PROFILEIMAGE: String
    let INITDTTM : Date
    
    var timeAgo: String {
        let formatter = RelativeDateTimeFormatter()
        formatter.unitsStyle = .abbreviated
        return formatter.localizedString(for: INITDTTM, relativeTo: Date())
    }
}

//

struct ImageInfo: Codable, Identifiable {
    let IMAGE_ID: Int
    
    var id: Int { IMAGE_ID }
}
