//
//  FeedTestView.swift
//  Halo
//
//  Created by 김병일 on 2022/10/29.
//

import SwiftUI

struct FeedTestView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct FeedTestView_Previews: PreviewProvider {
    static var previews: some View {
        FeedTestView()
    }
}
