//
//  CreateNewMessageView.swift
//  Halo
//
//  Created by 김병일 on 2022/08/03.
//

import SwiftUI
import SDWebImageSwiftUI


class CreateNewMessageViewModel: ObservableObject {
    
    //@Published var users = [ChatUser]()  //firebase
    
    @Published var users = [User]()  // nodejs
    
    @Published var errorMessage = ""
    
    init() {
      //  fetchAllUsers()  //firebase
        fetchFriendUsers_nodejs()
    }
    
    func fetchFriendUsers_nodejs() {
       Service.shared.fetchFriendUsers_nodejs { (res) in
           switch res {
           case .failure(let err):
               print("Failed to get allUserList" , err)
           case .success(let friendusers):
               print("friendusers")
              //print(friendusers)
               self.users = friendusers
           }
       }
   }
    
//    private func fetchAllUsers() {
//        FirebaseManager.shared.firestore.collection("users")
//            .getDocuments { documentsSnapshot, error in
//                if let error = error {
//                    self.errorMessage = "Failed to fetch users: \(error)"
//                    print("Failed to fetch users: \(error)")
//                    return
//                }
//
//                documentsSnapshot?.documents.forEach({ snapshot in
//                    let user = try? snapshot.data(as: ChatUser.self)
//                    if user?.uid != FirebaseManager.shared.auth.currentUser?.uid {
//                        self.users.append(user!)
//                    }
//
//                })
//            }
//    }
}
 
struct CreateNewMessageView: View {
    
    //let didSelectNewUser : (ChatUser) -> ()  //firebase
    
    let didSelectNewUser : (User) -> ()  
    
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var vm = CreateNewMessageViewModel()
    
    var body: some View {    
        NavigationView {
            ScrollView {
                Text(vm.errorMessage)
                ForEach(vm.users , id: \.NAME){ user in 
                    Button {
                        presentationMode.wrappedValue.dismiss()
                        didSelectNewUser(user)
                    } label: {
                        
                        HStack(spacing : 16){
                            WebImage(url: URL(string: user.PROFILEIMAGE))
                                .resizable()
                                .scaledToFill()
                                .frame(width: 50, height: 50)
                                .clipped()
                                .cornerRadius(50)
                                .overlay(RoundedRectangle(cornerRadius: 50)
                                    .stroke(Color(.label), lineWidth: 1 ))
                            Text(user.NAME)
                            Spacer()
                        }.padding(.horizontal)
                        Divider().padding(.vertical, 8)
                    }
                }
            }.navigationTitle("New Message")
                .toolbar {
                    ToolbarItemGroup(placement: .navigationBarLeading){
                        Button{
                            presentationMode.wrappedValue.dismiss()
                        } label : {
                            Text("Cancel")
                        }
                    }
                }
        }
    }
}

struct CreateNewMessageView_Previews: PreviewProvider {
    static var previews: some View {
//        CreateNewMessageView()
        if #available(iOS 16.0, *) {
            MainMessagesView()
        } else {
            // Fallback on earlier versions
        }
    }
}
