//
//  ProfileView.swift
//  Halo
//
//  Created by 김병일 on 2022/10/21.
//

import SwiftUI

struct ProfileView: View {
    var body: some View {
        NavigationView {
            ScrollView {
                Text("profile view ").font(.system(size: 30))
              
                    Image(systemName: "person.fill")
                        .font(.system(size: 64))
                        .padding()
                
                Button {
                    
                } label: {
                    Text("User Edit").foregroundColor(.white)
                        .background(Color.blue)
                }
               
               
            }
        }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}
