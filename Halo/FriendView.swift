//
//  FriendView.swift
//  Halo
//
//  Created by 김병일 on 2022/12/23.
//

import SwiftUI
import SDWebImageSwiftUI


class FriendViewModel: ObservableObject {
    
    
    @Published var users = [User]()  // nodejs
    
    @Published var errorMessage = ""
    
    init() {
        fetchFriendUsers_nodejs()
    }
    
    func fetchFriendUsers_nodejs() {
       Service.shared.fetchFriendUsers_nodejs { (res) in
           switch res {
           case .failure(let err):
               print("Failed to get allUserList" , err)
           case .success(let friendusers):
               print("friendusers")
              //print(friendusers)
               self.users = friendusers
           }
       }
   }

}

struct FriendView: View {
    
    let didSelectNewUser : (User) -> ()
    
    @ObservedObject var vm = FriendViewModel()
    
    var body: some View {
            ScrollView {
                Text(vm.errorMessage)
                ForEach(vm.users , id: \.NAME){ user in
                    Button {
                        didSelectNewUser(user  )
                    } label: {
                        
                        HStack(spacing : 16){
                            WebImage(url: URL(string: user.PROFILEIMAGE))
                                .resizable()
                                .scaledToFill()
                                .frame(width: 50, height: 50)
                                .clipped()
                                .cornerRadius(50)
                                .overlay(RoundedRectangle(cornerRadius: 50)
                                    .stroke(Color(.label), lineWidth: 1 ))
                            Text(user.NAME)
                            Spacer()
                        }.padding(.horizontal)
                        Divider().padding(.vertical, 8)
                    }
                }
            }
    }
}


struct FriendView_Previews: PreviewProvider {
    static var previews: some View {
        if #available(iOS 16.0, *) {
            MainMessagesView()
        } else {
            // Fallback on earlier versions
        }
    }
}
