//
//  FirebaseManager.swift
//  Halo
//
//  Created by 김병일 on 2022/06/25.
//

import Foundation

import Firebase
import FirebaseStorage
import FirebaseFirestore

class FirebaseManager: NSObject {
    
    let auth : Auth
    let storage : Storage
    let firestore : Firestore
    
    var currentUser: ChatUser?
    
    static let shared = FirebaseManager()
    
    override init() {
        FirebaseApp.configure()
        
        self.auth = Auth.auth()
        self.storage = Storage.storage()
        self.firestore = Firestore.firestore()
        
        super.init()
    }
}
 
