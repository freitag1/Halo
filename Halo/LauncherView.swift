//
//  LauncherView.swift
//  Halo
//
//  Created by 김병일 on 2022/11/11.
//

import SwiftUI

struct LauncherView: View {
    @State var progress: CGFloat = 0
       @State var doneLoading: Bool = false
        
       var body: some View {
           ZStack {
               if doneLoading {
                   if #available(iOS 16.0, *) {
                       MainMessagesView()
                           .transition(AnyTransition.opacity.animation(.easeInOut(duration: 1.0)))
                   } else {
                       // Fallback on earlier versions
                   }
               } else {
                   LoadingView(content: Image("Halo_launcher")
                                           .resizable()
                                           .scaledToFit()
                                           .padding(.horizontal, 50),
                               progress: $progress)
                       // Added to simulate asynchronous data loading
                       .onAppear {
                           DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                               withAnimation {
                                   self.progress = 0
                               }
                               DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
                                   withAnimation {
                                       self.doneLoading = true
                                   }
                                    
                               }
                           }
                       }
               }
           }
       }
}

struct LauncherView_Previews: PreviewProvider {
    static var previews: some View {
        LauncherView()
    }
}

struct ScaledMaskModifier<Mask: View>: ViewModifier {
     
    var mask: Mask
    var progress: CGFloat
     
    func body(content: Content) -> some View {
        content
            .mask(GeometryReader(content: { geometry in
                self.mask.frame(width: self.calculateSize(geometry: geometry) * self.progress,
                                height: self.calculateSize(geometry: geometry) * self.progress,
                                alignment: .center)
            }))
    }
     
    // Calculate Max Size of Mask
    func calculateSize(geometry: GeometryProxy) -> CGFloat {
        if geometry.size.width > geometry.size.height {
            return geometry.size.width
        }
        return geometry.size.height
    }
 
}
 
struct LoadingView<Content: View>: View {
 
    var content: Content
    @Binding var progress: CGFloat
    @State var logoOffset: CGFloat = 0 //Animation Y Offset
     
    var body: some View {
        content
            .modifier(ScaledMaskModifier(mask: Circle(), progress: progress))
            .offset(x: 0, y: logoOffset)
            .onAppear {
                withAnimation(Animation.easeInOut(duration: 1)) {
                    self.progress = 1.0
                }
                withAnimation(Animation.easeInOut(duration: 0.4).repeatForever(autoreverses: true)) {
                    self.logoOffset = 10
                }
            }
    }
}
